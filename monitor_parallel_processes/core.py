import logging
import json
import os.path
import sys
import time

from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *


logger = logging.getLogger(__name__)

def processes_description(json_file):
    with open(json_file) as file:
        data = json.load(file)
    return data

class ClickableLabel(QLabel):
    # Custom signal
    clicked = pyqtSignal()

    def __init__(self, text):
        super().__init__(text)

    def mousePressEvent(self, event):
        if event.button() == Qt.LeftButton:  # Ensure it's a left-click
            self.clicked.emit()  # Emit the signal when the label is clicked

TEXT_COLOR_GREEN = QColor("green")
TEXT_COLOR_DEFAULT = QApplication.palette().color(QPalette.WindowText)
LABEL_BACKGROUND_COLOR_SELECTED = QColor("lightblue")

def get_label_text_color(label: QLabel) -> QColor:
    return label.palette().color(label.foregroundRole())

def set_label_text_color(label: QLabel, color: QColor):
    palette = label.palette()
    palette.setColor(label.foregroundRole(), color)
    label.setPalette(palette)

def get_label_background_color(label: QLabel) -> QColor:
    return label.palette().color(label.backgroundRole())

def set_label_background_color(label: QLabel, color: QColor):
    palette = label.palette()
    palette.setColor(label.backgroundRole(), color)
    label.setAutoFillBackground(True)  # Ensure the background color is filled
    label.setPalette(palette)

def reset_label_background_color(label: QLabel):
    dummy_label = QLabel()
    default_background_color = get_label_background_color(dummy_label)
    set_label_background_color(label, default_background_color)

def get_tab_text_color(tab_widget: QTabWidget, index: int) -> QColor:
    tab_bar = tab_widget.tabBar()
    return tab_bar.tabTextColor(index)

def set_tab_text_color(tab_widget: QTabWidget, index: int, color: QColor):
    tab_bar = tab_widget.tabBar()
    tab_bar.setTabTextColor(index, color)


class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()

        # TODO
        #self.setWindowFlags(self.windowFlags() & ~Qt.WindowCloseButtonHint)

        logo_path = os.path.join(os.path.dirname(__file__), 'logo.png')
        self.setWindowIcon(QIcon(logo_path))
        self.setWindowTitle("Monitor parallel processes")
        self.setGeometry(100, 100, 1400, 800)
        
        # Add a status bar to the main window
        self.statusBar = QStatusBar()
        self.setStatusBar(self.statusBar)

        # Create the central widget
        self.central_widget = QWidget()
        self.setCentralWidget(self.central_widget)

        # Create the upper zone widget with a grid layout
        # Create the tab widget to display subprocess output
        self.left_std_widget = QWidget()
        self.right_tab_widget = QTabWidget()

        # Configure
        self.right_tab_widget.currentChanged.connect(self.current_tab_changed)
        self.right_tab_widget.setTabsClosable(True)
        self.right_tab_widget.tabCloseRequested.connect(self.tab_close_requested)

        # Create a layout for the central widget and add the upper zone widget and the tabs widget
        self.central_widget_layout = QHBoxLayout(self.central_widget)
        self.central_widget_layout.addWidget(self.left_std_widget, 0)
        self.central_widget_layout.addWidget(self.right_tab_widget, 1)

        # Scroll area for the top layout
        scroll_area = QScrollArea()
        scroll_area.setWidgetResizable(True)

        # Add scroll area to the top layout
        top_layout = QVBoxLayout(self.left_std_widget)
        top_layout.addWidget(scroll_area)

        # Widget for inside the scroll area
        scroll_content = QWidget()
        scroll_area.setWidget(scroll_content)
        self.left_std_widget_grid_layout = QGridLayout(scroll_content)

        # Animation
        self.timer = QTimer()
        self.timer.timeout.connect(self.status_timer_is_elapsed)
        self.timer.start(1000)
        self.blinking_status = False

        # Initialize internal variables
        self.std_widgets_as_tab = []
        self.stdout_text_boxes = []
        self.buttons = []
        self.commands = []
        self.names = []
        self.processes = []
        self.names_labels = []

        self.number_of_processes = 0

    def add_process(self, name, command):
        # Get process index
        process_index = self.number_of_processes
        self.number_of_processes += 1

        # Create label widgets
        control_button = QPushButton("Start")
        name_label = ClickableLabel(name)

        # Configuration
        control_button.clicked.connect(lambda: self.start_process(process_index))
        name_label.clicked.connect(lambda: self.on_name_label_click(process_index))

        # Add to the upper zone
        self.left_std_widget_grid_layout.addWidget(control_button, process_index, 0)
        self.left_std_widget_grid_layout.addWidget(name_label, process_index, 1)

        # Create a new QTextEdit for stdout and add it to the tab widget
        stdout_text_edit = QTextEdit()
        stdout_text_edit.setFont(QFont('DejaVu Sans Mono', 8))
        stdout_text_edit.setLineWrapColumnOrWidth(2000) #Here you set the width you want
        stdout_text_edit.setLineWrapMode(QTextEdit.LineWrapMode.FixedPixelWidth)

        # Add to the internal lists
        self.std_widgets_as_tab.append(None)
        self.stdout_text_boxes.append(stdout_text_edit)
        self.buttons.append(control_button)
        self.commands.append(command)
        self.names.append(name)
        self.processes.append(None)
        self.names_labels.append(name_label)

    def start_process(self, process_index):
        name = self.names[process_index]
        logger.debug(f"Starting process: {name}")

        # Create a tab widget
        std_widget_as_tab = self.std_widgets_as_tab[process_index]
        if std_widget_as_tab is None:
            std_widget_as_tab = QWidget()
            std_widget_as_tab.process_index = process_index

            tab_layout = QVBoxLayout(std_widget_as_tab)
            tab_layout.addWidget(self.stdout_text_boxes[process_index])
            
            index = self.right_tab_widget.addTab(std_widget_as_tab, name)
            self.right_tab_widget.setCurrentIndex(index)

            self.std_widgets_as_tab[process_index] = std_widget_as_tab

        # Create a new QProcess
        new_process = QProcess(self)

        # Configure the process to read stdout
        new_process.setProcessChannelMode(QProcess.MergedChannels)
        new_process.readyReadStandardOutput.connect(lambda: self.read_stdout(process_index))
        
        # Declare the function to execute when the process is finished 
        new_process.finished.connect(lambda: self.process_finished(process_index))

        # Start the process
        new_process.start(self.commands[process_index])

        # Empty the log
        self.stdout_text_boxes[process_index].setText('')

        # Configure the button for stop
        self.configure_button(process_index, "stop")

        # Store the new process
        self.processes[process_index] = new_process
        
        # Display a message in the status bar
        self.statusBar.showMessage(f"Process started: {name}")

    def on_name_label_click(self, process_index):
        index, tab_widget = self.get_tab(process_index)
        if index is not None:
            self.right_tab_widget.setCurrentIndex(index)

    def stop_process(self, process_index):
        name = self.names[process_index]
        logger.debug(f"Stopping process: {name}")

        # Terminate the subprocess
        self.processes[process_index].kill()

    def configure_button(self, process_index, action):
        if action == "start":
            # Change the button label
            self.buttons[process_index].setText("Start")
            # Change the function to execute when the user will click on the button
            self.buttons[process_index].clicked.disconnect()
            self.buttons[process_index].clicked.connect(lambda: self.start_process(process_index))
        else:
            # Change the button label
            self.buttons[process_index].setText("Stop")
            # Change the function to execute when the user will click on the button
            self.buttons[process_index].clicked.disconnect()
            self.buttons[process_index].clicked.connect(lambda: self.stop_process(process_index))

    def read_stdout(self, process_index):
        # Read subprocess output and display it in the QTextEdit
        output = self.processes[process_index].readAllStandardOutput().data().decode().strip()
        if output:
            self.stdout_text_boxes[process_index].append(output)

    def status_timer_is_elapsed(self):
        self.timer.start(1000)
        if self.blinking_status == False:
            self.blinking_status = True
        else:
            self.blinking_status = False
        
        # Define the color of the text regarding the blinking status
        if self.blinking_status:
            color = TEXT_COLOR_GREEN
        else:
            color = TEXT_COLOR_DEFAULT
        
        # Change the color of the name labels
        for process_index in range(self.number_of_processes):
            if self.processes[process_index] is not None:
                label = self.names_labels[process_index]
                set_label_text_color(label, color)

        # Change the color of the tabs
        for process_index in range(self.number_of_processes):
            if self.processes[process_index] is not None:
                index, tab_widget = self.get_tab(process_index)
                if index is not None:
                    set_tab_text_color(self.right_tab_widget, index, color)
    
    def current_tab_changed(self, index):
        tab_widget = self.right_tab_widget.widget(index)
        if tab_widget is not None:
            process_index = tab_widget.process_index
            name = self.names[process_index]
            logger.debug(f"Current tab changed: {name}")
            
            # Change the color of the name labels
            for i, label in enumerate(self.names_labels):
                if i == process_index:
                    set_label_background_color(label, LABEL_BACKGROUND_COLOR_SELECTED)
                else:
                    reset_label_background_color(label)
        else:
            logger.debug("Current tab changed: None")

    def tab_close_requested(self, index):
        process_index = self.right_tab_widget.widget(index).process_index
        name = self.names[process_index]
        logger.debug(f"Tab close requested: {name}")

        if self.processes[process_index] is not None:
            # Check if the process is running
            if self.processes[process_index].state() == QProcess.Running:
                # Inform the user that the process is still running
                # Ask the user if they want to stop the process as well as closing the tab,
                # close the tab without stopping the process, or abort the tab closing
                msg_box = QMessageBox()
                msg_box.setIcon(QMessageBox.Question)
                msg_box.setWindowTitle("Confirm Close")
                msg_box.setText(f"The process '{name}' is still running. What would you like to do?")
                msg_box.setStandardButtons(QMessageBox.Yes | QMessageBox.No | QMessageBox.Cancel)
                msg_box.setDefaultButton(QMessageBox.Cancel)
                msg_box.button(QMessageBox.Yes).setText("Stop Process and Close Tab")
                msg_box.button(QMessageBox.No).setText("Close Tab Only")
                msg_box.button(QMessageBox.Cancel).setText("Abort")

                ret = msg_box.exec_()

                if ret == QMessageBox.Yes:
                    self.stop_process(process_index)
                    self.right_tab_widget.removeTab(index)
                    self.std_widgets_as_tab[process_index] = None
                elif ret == QMessageBox.No:
                    self.right_tab_widget.removeTab(index)
                    self.std_widgets_as_tab[process_index] = None
                # If Cancel, do nothing
        else:
            self.right_tab_widget.removeTab(index)
            self.std_widgets_as_tab[process_index] = None
            
    def process_finished(self, process_index):
        name = self.names[process_index]
        exit_code = self.processes[process_index].exitCode()

        logger.debug(f"Process finished: {name}")
        
        # Display a message in the status bar
        self.statusBar.showMessage(f"Process finished: {name}")

        # Change the color of the name label
        label = self.names_labels[process_index]
        set_label_text_color(label, TEXT_COLOR_DEFAULT)

        # Change the color of the tab
        index, tab_widget = self.get_tab(process_index)
        if index is not None:
            set_tab_text_color(self.right_tab_widget, index, TEXT_COLOR_DEFAULT)        

        # Erase the process
        self.processes[process_index] = None
        
        # Configure the button for start
        self.configure_button(process_index, "start")

    def get_tab(self, process_index):
        for index in range(self.right_tab_widget.count()):
            tab_widget = self.right_tab_widget.widget(index)
            if tab_widget.process_index == process_index:
                return index, tab_widget
        return None, None

    def closeEvent(self, event):
        # Terminate all running processes before closing
        for index, process in enumerate(self.processes):
            if process is not None:
                self.stop_process(index)
        for index, process in enumerate(self.processes):
            if process is not None:
                process.waitForFinished()
        event.accept()

def run(config_json_file):
    app = QApplication(sys.argv)
    window = MainWindow()
    for process in processes_description(config_json_file):
        window.add_process(process['name'], process['command'])
    window.show()
    sys.exit(app.exec())

if __name__ == "__main__":
    run(sys.argv[1])