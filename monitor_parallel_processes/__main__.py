import click
import logging
import sys

import monitor_parallel_processes.core

@click.group()
@click.option(
    '-v', '--verbose', count=True,
    help="Increase verbosity level: -v for INFO, -vv for DEBUG."
)
@click.pass_context
def cli(ctx, verbose):
    """'monipp' stands for 'Monitor Parallel Process."""
    ctx.ensure_object(dict)
    # Store verbosity level in the context object
    ctx.obj['VERBOSE'] = verbose
    setup_logging(verbose)

@cli.command()
@click.option('-c', '--config', type=click.Path(exists=True), required=True, help="Path to the configuration file.")
@click.pass_context
def run(ctx, config):
    """Run the main operation using the specified configuration file."""
    monitor_parallel_processes.core.run(config)

def setup_logging(verbose_level):
    # Set the logging level based on the verbosity
    if verbose_level >= 2:
        level = logging.DEBUG
    elif verbose_level == 1:
        level = logging.INFO
    else:
        level = logging.WARNING

    logging.basicConfig(
        level=level,
        format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
        handlers=[
            logging.StreamHandler(sys.stdout)
        ]
    )

def main():
    cli(obj={})

if __name__ == "__main__":
    main()
