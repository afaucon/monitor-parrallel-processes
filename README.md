# Monitor Parallel Processes

This project provides a tool to monitor and manage parallel processes.

## Table of Contents

- [Installation](#installation)
- [Usage](#usage)
- [Configuration](#configuration)
- [Contributing](#contributing)
- [License](#license)

## Installation

To install the tool, run:

```bash
pip install .
```

## Usage

### Describe the list of processes

Example:

```json
[
    {
        "name": "Server",
        "working_dir": "$HOME",
        "command": "java -jar server.jar"
    },
    {
        "name": "Client 1",
        "working_dir": "$HOME",
        "command": "java -jar client1.jar"
    },
    {
        "name": "Client 2",
        "working_dir": "$HOME",
        "command": "python -m client2"
    }
]
```

Save it as `process_descriptor.json`.

### Run

To run the tool, use:

```bash
monipp -vv run -c process_descriptor.json
```

## Contributing

Contributions are welcome! Please open an issue or submit a pull request.

## License

This project is licensed under the MIT License.